<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $current_timestamp = date( 'Y-m-d H:i:s' );
        //
        DB::table( 'categorias' )->insert( [ #1
            'nombre' => 'Lacteos',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'categorias' )->insert( [ #2
            'nombre' => 'Frutas',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'categorias' )->insert( [ #3
            'nombre' => 'Verduras',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'categorias' )->insert( [ #4
            'nombre' => 'Consumo',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'categorias' )->insert( [ #5
            'nombre' => 'Medicina',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
    }
}
