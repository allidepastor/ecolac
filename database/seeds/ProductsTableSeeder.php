<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(  )
    {
        // 
        $current_timestamp = date( 'Y-m-d H:i:s' );
        //
        DB::table( 'productos' )->insert( [ #1
            'tipo_id' => 1,
            'categoria_id' => 1,        
            'nombre' => 'Leche',
            'descripcion' => 'Leche',
            'nota' => 'Leche',
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'productos' )->insert( [ #2
            'tipo_id' => 3,
            'categoria_id' => 1,        
            'nombre' => 'Queso',
            'descripcion' => 'Queso',
            'nota' => 'Queso',
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'productos' )->insert( [ #3
            'tipo_id' => 5,
            'categoria_id' => 1,        
            'nombre' => 'Dulce de Leche',
            'descripcion' => 'Dulce de Leche',
            'nota' => 'Dulce de Leche',
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'productos' )->insert( [ #4
            'tipo_id' => 7,
            'categoria_id' => 1,        
            'nombre' => 'Yogurts',
            'descripcion' => 'Yogurts',
            'nota' => 'Yogurts',
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
    }
}
