<?php

use Illuminate\Database\Seeder;

class MedidasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $current_timestamp = date( 'Y-m-d H:i:s' );
        //
        DB::table( 'medidas' )->insert( [ #1
            'nombre' => 'Gramos',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'medidas' )->insert( [ #2
            'nombre' => 'Kilogramos',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'medidas' )->insert( [ #3
            'nombre' => 'Onza',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'medidas' )->insert( [ #4
            'nombre' => 'Libras',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'medidas' )->insert( [ #5
            'nombre' => 'Litro',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'medidas' )->insert( [ #6
            'nombre' => 'Galon',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'medidas' )->insert( [ #7
            'nombre' => 'Unidades',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );    
    }
}
