<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TiposTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(PresentacionsTableSeeder::class);
        $this->call(MedidasTableSeeder::class);
        $this->call(InventariosTableSeeder::class);
    }
}
