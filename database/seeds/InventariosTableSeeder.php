<?php

use Illuminate\Database\Seeder;

class InventariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $current_timestamp = date( 'Y-m-d H:i:s' );
        //
        DB::table( 'inventarios' )->insert( [ #1
            'producto_id' => 1,
            'presentacion_id' => 2,
            'medida_id' => 5,        
            'tamaño' => 1,
            'precio' => '1',
            'imagen_url' => '1578849018leche000.jpg',
            'existencia' => '20',
            'disponible' => '20',
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'inventarios' )->insert( [ #2
            'producto_id' => 2,
            'presentacion_id' => 2,
            'medida_id' => 5,        
            'tamaño' => 1,
            'precio' => '1',
            'imagen_url' => '1578849059queso.jpg',
            'existencia' => '20',
            'disponible' => '20',
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'inventarios' )->insert( [ #3
            'producto_id' => 3,
            'presentacion_id' => 2,
            'medida_id' => 5,        
            'tamaño' => 1,
            'precio' => '1',
            'imagen_url' => '1578849256dulces.jpg',
            'existencia' => '20',
            'disponible' => '20',
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'inventarios' )->insert( [ #4
            'producto_id' => 4,
            'presentacion_id' => 1,
            'medida_id' => 5,        
            'tamaño' => 1,
            'precio' => '1',
            'imagen_url' => '1578849306yogut.jpg',
            'existencia' => '20',
            'disponible' => '20',
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
    }
}
