<?php

use Illuminate\Database\Seeder;

class RolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $current_timestamp = date( 'Y-m-d H:i:s' );
        //
        DB::table( 'rols' )->insert( [ #1
            'nombre' => 'admin',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'rols' )->insert( [ #2
            'nombre' => 'administrador',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'rols' )->insert( [ #3
            'nombre' => 'repartidor',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'rols' )->insert( [ #4
            'nombre' => 'vendedor',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'rols' )->insert( [ #5
            'nombre' => 'cliente',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
    }
}
