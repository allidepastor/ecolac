<?php

use Illuminate\Database\Seeder;

class PresentacionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $current_timestamp = date( 'Y-m-d H:i:s' );
        //
        DB::table( 'presentacions' )->insert( [ #1
            'nombre' => 'Botella',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'presentacions' )->insert( [ #2
            'nombre' => 'Bolsa',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'presentacions' )->insert( [ #3
            'nombre' => 'Caja',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'presentacions' )->insert( [ #4
            'nombre' => 'Cajilla',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'presentacions' )->insert( [ #5
            'nombre' => 'Ninguna',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
    }
}
