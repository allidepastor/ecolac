<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(  )
    {
        //
        $current_timestamp = date( 'Y-m-d H:i:s' );
        //
        DB::table( 'users' )->insert( [
            'nombre' => 'Allison',
            'apellido' => 'Pastor',
            'cedula' => '0931481817',
            'telefono' => '12345678',
            'email' => 'allidepastor@gmail.com',
            'password' => bcrypt('admin'),
            'rol_id' => 1,
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'users' )->insert( [
            'nombre' => 'Fernando',
            'apellido' => 'Ordoñez',
            'cedula' => '0959147020',
            'telefono' => '12345678',
            'email' => 'fer@gmail.com',
            'password' => bcrypt('admin'),
            'rol_id' => 2,
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'users' )->insert( [
            'nombre' => 'Genesis',
            'apellido' => 'Pastor',
            'cedula' => '0931481817',
            'telefono' => '12345678',
            'email' => 'genesis@live.com',
            'password' => bcrypt('admin'),
            'rol_id' => 3,
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'users' )->insert( [
            'nombre' => 'Maria',
            'apellido' => 'Feijoo',
            'cedula' => '0931481817',
            'telefono' => '12345678',
            'email' => 'maria@gmail.com',
            'password' => bcrypt('admin'),
            'rol_id' => 4,
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
        DB::table( 'users' )->insert( [
            'nombre' => 'Laura',
            'apellido' => 'Sotomayor',
            'cedula' => '909090909',
            'telefono' => '12345678',
            'email' => 'laura@live.com',
            'password' => bcrypt('admin'),
            'rol_id' => 5,
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
    }
}
