<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(  )
    {
        //
        $current_timestamp = date( 'Y-m-d H:i:s' );
        //
        DB::table( 'tipos' )->insert( [ #1
            'nombre' => 'Entera',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
         DB::table( 'tipos' )->insert( [ #2
            'nombre' => 'Deslactosada',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
         DB::table( 'tipos' )->insert( [ #3
            'nombre' => 'Normal',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
         DB::table( 'tipos' )->insert( [ #4
            'nombre' => 'Mozzarella',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
         DB::table( 'tipos' )->insert( [ #5
            'nombre' => 'Dulce de Leche',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
         DB::table( 'tipos' )->insert( [ #6
            'nombre' => 'Frutilla',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
         DB::table( 'tipos' )->insert( [ #7
            'nombre' => 'Mango',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
         DB::table( 'tipos' )->insert( [ #8
            'nombre' => 'Guayaba',            
            'created_at' => $current_timestamp,
            'updated_at' => $current_timestamp
        ] );
         
    }
}
