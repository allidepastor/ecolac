<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccions', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->unsignedBigInteger('user_id')->nullable(  );
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('nombre')->nullable(  );
            $table->string('email', 250)->nullable(  );
            $table->string('latitud')->nullable(  );
            $table->string('longitud')->nullable(  );
            $table->string('ciudad')->nullable(  );
            $table->string('direccion')->nullable(  );
            $table->boolean('default');
            $table->boolean('estado');
            $table->timestamps(  );
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccions');
    }
}
