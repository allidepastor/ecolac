<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('nombre')->nullable(  );         
            $table->unsignedBigInteger('user_id')->nullable(  );
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('latitud')->nullable(  );
            $table->string('longitud')->nullable(  );
            $table->string('ciudad')->nullable(  );
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursals');
    }
}
