<?php

Route::get( '/', 'HomeController@index' );
Route::get( '/carrito', 'HomeController@carrito' );
Route::get( '/procesando', 'HomeController@procesando' );

// Cart Actions
Route::get( '/add/cart/{id}', 'HomeController@add_cart' ); // localhost:8000/store/cart
Route::get( '/update/cart/quantity/{id}/{q}', 'HomeController@update_cart_quantity' ); // localhost:8000/store/cart
Route::get( '/count/{id}/orders', 'HomeController@get_orders' ); // localhost:8000/store/cart
Route::get( '/delete/cart/{id}', 'HomeController@delete_order_detail' ); // localhost:8000/store/cart
Route::get('/check/{id}/order', 'HomeController@check_order');
 
//REPORTE INVENTARIO
/*Route::get('/administrador/inventarios', 'AdministradorController@list_inventarios', function () {
    $pdf = PDF::loadview('');
    $pdf-> setPaper('a4','landscape');
    return $pdf->stream();
});*/
//FIN REPORTE

Route::get('/logout', 'Auth\LoginController@logout');  // localhost:8000/logout <-- para cerrar sesion

Route::resource('trainer','TrainerController');
Route::resource('direccion','DireccionController');
Route::resource('productos','ProductoController');
Route::resource('inventarios','InventarioController');
Route::resource('usuarios','UsuarioController');
Route::resource('categorias','CategoriaController');
Route::resource('medidas','MedidaController');
Route::resource('presentacions','PresentacionController');
Route::resource('tipos','TipoController');
Route::resource('sucursals','SucursalController');
Route::resource('configs','ConfigController');
Route::resource( 'pedido', 'PedidoController' );
Route::resource( 'pedido/detalle', 'PedidoDetalleController' );



Auth::routes();
/*---------- Inicio de URLs para los Clientes -------------*/
//Route::get('/', 'GeneralController@index');
Route::get('/home', 'HomeController@index')->name('home');

/*---------- Fin de URLs para los Clientes -------------*/

/*---------- Inicio de URLs para el Admin -------------*/
Route::get( '/admin', 'AdminController@index' )->name( 'admin' ); // localhost:8000/admin <- Para cargar la vista del home del usuario admin
Route::get( '/admin/users', 'AdminController@list_users' ); // localhost:8000/admin/users <- Para listar a todos los usuarios
Route::get( '/admin/user/create', 'AdminController@create_user' ); // localhost:8000/admin/user/create <- para crear usuarios
Route::get( '/admin/product/edit/{id}', 'AdminController@edit_product' ); // localhost:8000/admin/product/edit/1 <- para modificar los productos
Route::get( '/admin/products', 'AdminController@list_products' ); // localhost:8000/admin/products <- Para listar a todos los productos
Route::get( '/admin/product/create', 'AdminController@create_product' ); // localhost:8000/admin/product/create <- para crear productos
Route::get( '/admin/product/edit/{id}', 'AdminController@edit_product' ); // localhost:8000/admin/product/edit/1 <- para modificar los productos
Route::get( '/admin/configs', 'AdminController@list_configs' ); // localhost:8000/admin/products <- Para listar a todos los productos
Route::get( '/admin/config/edit/{id}', 'AdminController@edit_config' ); // localhost:8000/admin/product/edit/1 <- para modificar los productos

/*---------- Fin de URLs para el Admin -------------*/

/*---------- Inicio de URLs para los Administradores -------------*/
Route::get( '/administrador', 'AdministradorController@index' )->name( 'administrador' ); // localhost:8000/administrador
Route::get( '/administrador/products', 'AdministradorController@list_products' ); // localhost:8000/admin/products <- Para listar a todos los productos
Route::get( '/administrador/product/create', 'AdministradorController@create_product' ); // localhost:8000/admin/product/create <- para crear productos
Route::get( '/administrador/product/edit/{id}', 'AdministradorController@edit_product' ); // localhost:8000/admin/product/edit/1 <- para modificar los productos
Route::get( '/administrador/categorias', 'AdministradorController@list_categorias' ); 
Route::get( '/administrador/categoria/create', 'AdministradorController@create_categoria' ); 
Route::get( '/administrador/categoria/edit/{id}', 'AdministradorController@edit_categoria' );
//Route::get( '/administrador/categoria/delet/{id}', 'CategoriaController@destroy' ); 
Route::get( '/administrador/medidas', 'AdministradorController@list_medidas' ); 
Route::get( '/administrador/medida/create', 'AdministradorController@create_medida' ); 
Route::get( '/administrador/medida/edit/{id}', 'AdministradorController@edit_medida' ); 
Route::get( '/administrador/presentacions', 'AdministradorController@list_presentacions' ); 
Route::get( '/administrador/presentacion/create', 'AdministradorController@create_presentacion' ); 
Route::get( '/administrador/presentacion/edit/{id}', 'AdministradorController@edit_presentacion' ); 
Route::get( '/administrador/inventario/create', 'AdministradorController@create_inventario' ); 
Route::get( '/administrador/inventarios', 'AdministradorController@list_inventarios' );  
Route::get( '/administrador/inventario/edit/{id}', 'AdministradorController@edit_inventario' ); 
Route::get( '/administrador/tipo/create', 'AdministradorController@create_tipo' ); 
Route::get( '/administrador/tipos', 'AdministradorController@list_tipos' );  
Route::get( '/administrador/tipo/edit/{id}', 'AdministradorController@edit_tipo' ); 
Route::get( '/administrador/sucursal/create', 'AdministradorController@create_sucursal' ); 
Route::get( '/administrador/sucursals', 'AdministradorController@list_sucursals' );  
Route::get( '/administrador/sucursal/edit/{id}', 'AdministradorController@edit_sucursal' ); 

/*---------- Fin de URLs para los Administradores -------------*/

/*---------- Inicio de URLs para los Repartidores -------------*/
Route::get( '/repartidor', 'RepartidorController@index' )->name( 'repartidor' ); // localhost:8000/repartidor

/*---------- Fin de URLs para los Repartidores -------------*/

/*---------- Inicio de URLs para los vendedores -------------*/
Route::get( '/vendedor', 'VendedorController@index' )->name( 'vendedor' ); // localhost:8000/vendedor

/*---------- Fin de URLs para los vendedores -------------*/

