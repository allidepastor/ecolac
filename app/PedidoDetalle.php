<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoDetalle extends Model
{
    protected $table ='pedido_detalle';

    protected $guarded = [  ];

    public function pedido(  ) {
		return $this->belongsTo( 'App\Pedido' );
    }

    public function inventario(  ) {
		return $this->belongsTo( 'App\Inventario' );
    }
    
}
