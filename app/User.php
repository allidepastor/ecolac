<?php

namespace App;

use App\Rol;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public function roles(  )
    {
        return $this->belongsTo('App\Rol', 'rol_id');
        //return $this->belongsToMany( Rol::class )->withTimestamps(  );
    }
    public function sucursal(  )
    {
        
        return $this->belongsTo('App\sucursal');
        //$this->hasMany( User::class );
    }

    public function authorizeRoles($roles)
    {
    if($this->hasAnyRole($role))
        {
        return true;
        }
    abort(401,'Acción no autorizada');
    }

    public function hasAnyRole($roles)
    {
        if(is_array($roles))
        {
            foreach ($roles as $role)
            {
                if($this->hasRole($role))
                {
                    return true;
                }
            }
        }else
        {
            if($this->hasRole($roles))
            {
                return true;
            }
        }
    return false;
    }


    public function hasRole( $roleName ) {
        foreach ( $this->roles()->get() as $role ) {
            if ( $role->nombre == $roleName ) {
                return true;
            }
        }

        return false;
    }

    protected $table ='users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','apellido', 'cedula', 'telefono', 'email', 'password', 'rol_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}
