<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use App\Pedido;

class AppServiceProvider extends ServiceProvider
{
    use AuthenticatesUsers;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot( UrlGenerator $url )
    {
        View::composer( '*', function( $view ) {
            $sessionName = 'order_id';
            $order = [];
            //
            if ( \Session::has( $sessionName ) ) {
                $order_id = \Session::get($sessionName);
                $session_key = \Session::getId(  );
                $order = Pedido::findOrCreateById( intval( $order_id ), $session_key );
            } else { $order = Pedido::create( [ 'session' => '' ] ); }
           
            \Session::put( $sessionName, $order->id );
            $session_key = \Session::getId(  );
            Pedido::addSession( $order->id, $session_key );

            $view->with( 'order_data', [ (string)($order->id), $order->ordersCount(  ) ]  );
        } );
    }
}
