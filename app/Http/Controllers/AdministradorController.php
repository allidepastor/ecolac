<?php

namespace App\Http\Controllers;

use Auth;
use App\Categoria;
use App\Producto;
use App\Tipo;
use App\Inventario;
use App\Presentacion;
use App\Medida;
use App\Sucursal;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//use View;
class AdministradorController extends Controller
{

    public function __construct(  ) {
        //
        $this->middleware( 'auth' );
    }

    public function index (  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            return view( 'administrador.dashboard', compact( 'user' ) ); // se redirige a la vista de dashboard
        }
        return abort(404); // En caso que el usuario no tenga el rol de admin, se le manda a la pagina 404
    }

    // Funcion para cargar la vista de crear producto
    public function create_product(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $producto
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $categorias = Categoria::all(  );
            $tipos = Tipo::all(  );
            return view( 'administrador.crear_producto', compact( 'tipos','categorias' ) ); // se carga la vista que tiene el formulario para crear producto
        }
        return abort(404);
    }

    // Funcion para cargar la vista en la que se listan todos los productos
    public function list_products(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $categorias = Categoria::all(  );
            $tipos = Tipo::all(  );
            $productos = Producto::all(  );
            return view( 'administrador.lista_productos', compact( 'productos', 'tipos','categorias' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }
    
   // Funcion para cargar la vista con el formulario para editar un producto
    public function edit_product( Request $request, $id ) { // $id toma el parametro de la url /admin/user/edit/1 <- donde uno seria el $id
    $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
        // Aqui va el codigo si el usuario tiene el rol de Admin
        $categorias = Categoria::all(  );
        $tipos = Tipo::all(  );
        $producto = Producto::where( 'id', $id )->first();
        return view( 'administrador.editar_producto', compact( 'producto', 'tipos','categorias' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
         return abort(404);
    }

     // Funcion para cargar la vista de crear TIPOS
     public function create_tipo(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $producto
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin

            $tipos = Tipo::all(  );
            return view( 'administrador.crear_tipo', compact( 'tipos' ) ); // se carga la vista que tiene el formulario para crear producto
        }
        return abort(404);
    }

    // Funcion para cargar la vista en la que se listan todos los productos
    public function list_tipos(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            
            $tipos = Tipo::all(  );
            return view( 'administrador.lista_tipos', compact(  'tipos' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }
    
   // Funcion para cargar la vista con el formulario para editar un producto
    public function edit_tipo( Request $request, $id ) { // $id toma el parametro de la url /admin/user/edit/1 <- donde uno seria el $id
    $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
        // Aqui va el codigo si el usuario tiene el rol de Admin
        
        $tipo = Tipo::where( 'id', $id )->first();
        return view( 'administrador.editar_tipo', compact( 'tipo' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
         return abort(404);
    }
   
    // Funcion para cargar la vista de crear categoria
    public function create_categoria(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $producto
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $categorias = Categoria::all(  );
            return view( 'administrador.crear_categoria', compact('categorias' ) ); // se carga la vista que tiene el formulario para crear producto
        }
        return abort(404);
    }

    // Funcion para cargar la vista en la que se listan todos las categorias
    public function list_categorias(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $categorias = Categoria::all(  );
            return view( 'administrador.lista_categorias', compact('categorias' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }
    
   // Funcion para cargar la vista con el formulario para editar una categoria
    public function edit_categoria( Request $request, $id ) { // $id toma el parametro de la url /admin/user/edit/1 <- donde uno seria el $id
    $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
        // Aqui va el codigo si el usuario tiene el rol de Admin
        $categoria = Categoria::where('id',$id)->first();
        return view( 'administrador.editar_categoria', compact('categoria' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
         return abort(404);
    }

    // Funcion para cargar la vista de crear medida
    public function create_medida(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $producto
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $medidas = Medida::all(  );
            return view( 'administrador.crear_medida', compact('medidas' ) ); // se carga la vista que tiene el formulario para crear producto
        }
        return abort(404);
    }

    // Funcion para cargar la vista en la que se listan todos las categorias
    public function list_medidas(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $medidas = Medida::all(  );
            
            return view( 'administrador.lista_medidas', compact('medidas' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }
    
   // Funcion para cargar la vista con el formulario para editar una categoria
    public function edit_medida( Request $request, $id ) { // $id toma el parametro de la url /admin/user/edit/1 <- donde uno seria el $id
    $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
        // Aqui va el codigo si el usuario tiene el rol de Admin
        $medida = Medida::where( 'id', $id )->first();
        return view( 'administrador.editar_medida', compact('medida' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
         return abort(404);
    }

    // Funcion para cargar la vista de crear PRESENTACION
    public function create_presentacion(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $producto
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $presentacions = Presentacion::all(  );
            
            return view( 'administrador.crear_presentacion', compact( 'presentacions' ) ); // se carga la vista que tiene el formulario para crear producto
        }
        return abort(404);
    }

    // Funcion para cargar la vista en la que se listan todos los PRESENTACION
    public function list_presentacions(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $presentacions = Presentacion::all(  );
            return view( 'administrador.lista_presentacions', compact( 'presentacions' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }
    
   // Funcion para cargar la vista con el formulario para editar un producto
    public function edit_presentacion( Request $request, $id ) { // $id toma el parametro de la url /admin/user/edit/1 <- donde uno seria el $id
    $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
        // Aqui va el codigo si el usuario tiene el rol de Admin
        $presentacion = Presentacion::where( 'id', $id )->first();
        return view( 'administrador.editar_presentacion', compact( 'presentacion' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
         return abort(404);
    }

    // Funcion para cargar la vista de crear INVENTARIO
    public function create_inventario(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $producto
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $medidas = Medida::all();
            $productos = Producto::all();
            $presentacions = Presentacion::all();
            $inventarios=Inventario::all();
            $categorias = Categoria::all();
            $sucursals=Sucursal::all();
     
            return view( 'administrador.crear_inventario', compact( 'sucursals','categorias','inventarios',  'productos','medidas', 'presentacions' ) ); // se carga la vista que tiene el formulario para crear producto
        }
        return abort(404);
    }

    // Funcion para cargar la vista en la que se listan todos los productos
    public function list_inventarios(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $medidas = Medida::all();
            $productos = Producto::all();
            $presentacions = Presentacion::all();
            $inventarios=Inventario::all();
            $categorias = Categoria::all();
            $sucursals=Sucursal::all();
            return view( 'administrador.lista_inventarios', compact('sucursals', 'categorias','inventarios', 'productos','medidas', 'presentacions' ) ); // se carga la vista que tiene el formulario para crear producto
        
            
        }
        return abort(404);
    }
    
   // Funcion para cargar la vista con el formulario para editar un producto
    public function edit_inventario( Request $request, $id ) { 
    $user = Auth::user(  ); 
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
        // Aqui va el codigo si el usuario tiene el rol de Admin
            $medidas = Medida::all();
            $productos = Producto::all();
            $presentacions = Presentacion::all();
            $categorias = Categoria::all();
            $sucursals=Sucursal::all();
            $inventario= Inventario::where( 'id', $id )->first();
            return view( 'administrador.editar_inventario', compact( 'sucursals','categorias','inventario',  'productos','medidas', 'presentacions' ) ); // se carga la vista que tiene el formulario para crear producto
        }
         return abort(404);
    }
    
    // Funcion para cargar la vista de crear sucursal
    public function create_sucursal(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $producto
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            
        $users = User::where('rol_id', 3)->get();
            return view( 'administrador.crear_sucursal', compact( 'users') ); // se carga la vista que tiene el formulario para crear producto
        }
        return abort(404);
    }

    // Funcion para cargar la vista en la que se listan todos los productos
    public function list_sucursals(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            
            $sucursals= Sucursal::all();
            $users= User::all();

     
            return view( 'administrador.lista_sucursals', compact( 'sucursals', 'users') ); // se carga la vista que tiene el formulario para crear producto
        
            
        }
        return abort(404);
    }
    
   // Funcion para cargar la vista con el formulario para editar un producto
    public function edit_sucursal( Request $request, $id ) { 
    $user = Auth::user(  ); 
        if ( $user->hasRole('administrador') ) { // Se verifica que el usuario tiene el rol de Admin
        // Aqui va el codigo si el usuario tiene el rol de Admin
        $users = User::where('rol_id', 3)->get();
        $sucursals= Sucursal::where( 'id', $id )->first();
           
            return view( 'administrador.editar_sucursal', compact(  'users', 'sucursals') ); // se carga la vista que tiene el formulario para crear producto
        }
         return abort(404);
    }
}