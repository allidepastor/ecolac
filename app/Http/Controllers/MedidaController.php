<?php

namespace App\Http\Controllers;

use App\Medida;


use Illuminate\Http\Request;

class MedidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $medidas = Medida::all();
        //return view('productos.index',compact('productos'));
        if ( $request->wantsJson(  ) ) {
			return $medidas->toJson(  );
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $medidas = Medida::all();
        return view('medidas.create', compact('medidas'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        
        
        $medidas = new Medida();
        $medidas->nombre= $request->input('nombre');
        $medidas->notas  = $request->input('notas');
        
        $medidas->save();
        

        return redirect(  )->back(  )->with( 'success', 'Medida creada correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Medida $medida)// exlicit binding
    {
        //$producto = Producto::find($id);
       // $producto = Producto::where('slug','=', $slug)->firstOrFail();
      
        $medidas = Medida::all();

        return view('medidas.show',compact('medidas') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Medida $medida)
    {
        $medidas = Medida::all();
        return view('medidas.edit', compact('medida'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre' => 'required', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            'notas' => 'required',
            
            
        ] );
    
        $in_medida = Medida::where( 'id', $id )->update( [ // Se actualiza al usuario
            'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
            'notas' => $request->notas, // el nombre de la variable va de terminado por el nombre y id del input, ejemplo: el input de apellido es <input name="input" id="input".../>
         
        ] );            
         
        //$trainer->fill($request->all());
        /*$in_producto->fill($request->except('avatar'));
        $name="";
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $name = time().$file->getClientOriginalName();
            $in_producto->photo=$name;
            $file->move(public_path().'/images/',$name);
            
        }*/
        //$in_producto->save();
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado la medida correctamente' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $medida = Medida::find( $id ); // Buscamos al producto por el id
        $medidaname = $medida->nombre; // Guardamos el nombre del producto
        $medida->delete(  ); // eliminamos al producto
        return redirect(  )->back(  )->with( 'success', 'La medida ha sido eliminado correctamente ' ); // redirigimos a la pagina con un mensaje
    
    }
}
