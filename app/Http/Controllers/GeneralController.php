<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Categoria;
use App\Tipo;
use App\Producto;
use App\Inventario;
use App\Sucursal;
use App\Presentacion;
use App\Medida;

class GeneralController extends Controller
{
    public function index()
    {
        $inventarios = Inventario::all();
        if ( !Auth::guest(  ) ) {
            $user = Auth::user(  );
            if ( $user->hasRole('admin') ) {
                return redirect( '/admin' );
            } else if ( $user->hasRole('administrador') ) {
                return redirect( '/administrador' );
            } else if ( $user->hasRole('repartidor') ) {
                return redirect( '/repartidor' );
            } else if ( $user->hasRole('vendedor') ) {
                return redirect( '/vendedor' );
            } else { // Para clientes
                return view('welcome', compact('inventarios'));
            }
        } else {
            return view('welcome', compact('inventarios'));
        }
        
    }

}
