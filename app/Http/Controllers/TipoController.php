<?php

namespace App\Http\Controllers;

use App\Tipo;


use Illuminate\Http\Request;

class TipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $tipos = Tipo::all();
        //return view('productos.index',compact('productos'));
        if ( $request->wantsJson(  ) ) {
			return $tipos->toJson(  );
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tipos = Tipo::all();
        return view('tipos.create', compact('tipos'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        
        
        $tipos = new Tipo();
        $tipos->nombre= $request->input('nombre');
        
        $tipos->save();
        

        return redirect(  )->back(  )->with( 'success', 'Tipo creado correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tipo $tipo)// exlicit binding
    {
        //$producto = Producto::find($id);
       // $producto = Producto::where('slug','=', $slug)->firstOrFail();
      
        $tipos = Tipo::all();

        return view('tipos.show',compact('tipos') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tipo $tipo)
    {
        $tipos = Tipo::all();
        return view('tipos.edit', compact('tipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre' => 'required', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
 
            
        ] );
    
        $in_tipo = Tipo::where( 'id', $id )->update( [ // Se actualiza al usuario
            'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
         
        ] );            
         
        
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado el tipo correctamente' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $tipo = Tipo::find( $id ); // Buscamos al producto por el id
        $tiponame = $tipo->nombre; // Guardamos el nombre del producto
        $tipo->delete(  ); // eliminamos al producto
        return redirect(  )->back(  )->with( 'success', 'El tipo ha sido eliminado correctamente ' ); // redirigimos a la pagina con un mensaje
    
    }
}
