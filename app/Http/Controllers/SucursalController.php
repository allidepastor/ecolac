<?php

namespace App\Http\Controllers;

use App\Rol;
use App\User;
use App\Sucursal;

use Illuminate\Http\Request;

class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sucursals = Sucursal::all();
        //return view('productos.index',compact('productos'));
        if ( $request->wantsJson(  ) ) {
			return $productos->toJson(  );
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $users = User::where('role_id', 3)->get();
        return view('sucursals.create', compact('users'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        /*$name="";
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/',$name);
            
        }
        */
        $sucursals = new Sucursal();
        $sucursals->nombre= $request->input('nombre');
        $sucursals->user_id = $request->input('repartidor');
        $sucursals->ciudad  = $request->input('ciudad');
        $sucursals->longitud = $request->input('longitud');
        $sucursals->latitud = $request->input('latitud');
      
        $sucursals->save();
        

        return redirect(  )->back(  )->with( 'success', 'sucursal creada correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sucursal $sucursal)// exlicit binding
    {
        //$producto = Producto::find($id);
       // $producto = Producto::where('slug','=', $slug)->firstOrFail();
       $users = Tipo::all();
       $rols = Rol::all();
        return view('sucursals.show',compact('users', 'rols') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        $users = Tipo::all();
        $rols = Rol::all();
         return view('sucursals.edit',compact('users', 'rols') );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre' => 'required', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            'user_id' => 'required',
            'ciudad' => 'required',
            'longitud' => 'required',
            'latitud' => 'required',
           
        ] );
    
        $in_sucursal = Sucursal::where( 'id', $id )->update( [ // Se actualiza al usuario
            'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
            'user_id' => $request->user_id, // el nombre de la variable va de terminado por el nombre y id del input, ejemplo: el input de apellido es <input name="input" id="input".../>
            'ciudad' => $request->ciudad,
            'longitud' => $request->longitud,
            'latitud' => $request->latitud,
        ] );            
         
    
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado la sucursal correctamente' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $sucursal = Sucursal::find( $id ); // Buscamos al producto por el id
        $sucursalname = $sucursal->nombre; // Guardamos el nombre del producto
        $sucursal->delete(  ); // eliminamos al producto
        return redirect(  )->back(  )->with( 'success', 'la sucursal ha sido eliminado correctamente ' ); // redirigimos a la pagina con un mensaje
    
    }
}
