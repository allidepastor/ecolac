<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Pedido;
use App\Inventario;
use App\Direccion;
use App\PedidoDetalle;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $data;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');

        //Public Variables
        $this->data = array(  );

        /** Orders */
        $this->data['order_total'] = 0;
        $this->data['order_id'] = 0;
        $this->data['user_id'] = 0;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if ( !Auth::guest(  ) ) {
            $user = Auth::user(  );
            if ( $user->hasRole('admin') ) {
                return redirect( '/admin' );
            } else if ( $user->hasRole('administrador') ) {
                return redirect( '/administrador' );
            } else if ( $user->hasRole('repartidor') ) {
                return redirect( '/repartidor' );
            } else if ( $user->hasRole('vendedor') ) {
                return redirect( '/vendedor' );
            } else { // Para clientes
                $this->get_order_id( $user->id );
                return view('welcome')->with( $this->data );
            }
        } else {
            $this->get_order_id( 0 );
            return view('welcome')->with( $this->data );
        }
    }

    public function carrito(  ) {
        if ( !Auth::guest(  ) ) {
            $user = Auth::user(  );
            $this->data['user_id'] = $user->id;
            $this->get_order_id( $user->id );
        } else {  $this->get_order_id( 0 ); }
        return view('carrito')->with( $this->data );
    }

    public function procesando( ) {
        if ( !Auth::guest(  ) ) {
            $user = Auth::user(  );
            $this->data['user_id'] = $user->id;
            $this->get_order_id( $user->id );
        } else {  $this->get_order_id( 0 ); }
        return view('procesando')->with( $this->data );
    }

    // Get order ID
    protected function get_order_id( $user_id ) {
        //
        $result = 0;
        $count = 0;
        //
        $addresses = Direccion::where( 'user_id', $user_id )->get(  );
        $address_id = '';
        $check_orders = [];
        foreach( $addresses as $address ) {
            $check = Pedido::where( 'user_id', $user_id )->where( 'estado', 0 )->first(  );
            if ( $check ) {
                $check_orders = $check;
                $address_id = $address->id;
            }
        }
        if ( $check_orders ) {
            $result = $check_orders->id;
            $result = PedidoDetalle::where( 'pedido_id', $check_orders->id )->get(  );
            $count = $result->count(  );
        }
       
        //
        $this->data['order_total'] = $count;
        $this->data['order_id'] = $result;
    }

   // Add Product Cart
    protected function add_cart( Request $request, $id ) {
        //
        $user_id = Auth::user( )->id;
        $addresses = Direccion::where( 'user_id', $user_id )->get(  );
        $address_id = '';
        $check_orders = [];
        foreach( $addresses as $address ) {
            $check = Pedido::where( 'user_id', $user_id )->where( 'estado', 0 )->first(  );
            if ( $check ) {
                $check_orders = $check;
                $address_id = $address->id;
            }
        }

        $inventory = Inventario::where( 'id', $id )->first(  );

        $order_id;
        if ( $check_orders ) {
            $order_id = $check_orders->id;
        } else {
            $order_id = Pedido::insert( [
                "direccion_id" => $address_id,
                "estado" => 0
            ] );
        }

        PedidoDetalle::insert( [
            'pedido_id' => $order_id->id,
            'inventario_id' => $inventory->id,
            'cantidad' => 1,
            'precio' => $inventory->precio,
        ] );

        return json_encode( $order_id );
    }

    // Get Total Orders
    public function get_orders( Request $request, $id ) {
        //
        $result = PedidoDetalle::where( 'pedido_id', $id )->get(  );
        $count = $result->count(  );

        return json_encode( $count );

    }

    // Delete Order Item
    public function delete_order_detail( Request $request, $id ) {
        //
        $result = PedidoDetalle::where( 'id', $id )->delete(  );
        $delete = false;
        if ( $result ) { $delete = true; }

        return json_encode( [ $delete, 'Done' ] );

    }

    // Update order's item quantity
    public function update_cart_quantity( Request $request, $id, $q ) {
        //
        $result = PedidoDetalle::where('id', $id)->update([ 'cantidad' => $q ]);
        $update = false;
        if ( $result ) {
            $update = true;
        }
        return json_encode([ $update, 'Done' ]);
    }

    // Check if the item was added before
    public function check_order( Request $request, $id ) {
        $user_id = Auth::user(  )->id;
        $addresses = Direccion::where( 'user_id', $user_id )->get(  );
        $address_id = '';
        $check_orders = [];
        foreach( $addresses as $address ) {
            $check = Pedido::where( 'user_id', $user_id )->where( 'estado', 0 )->first(  );
            if ( $check ) {
                $check_orders = $check;
                $address_id = $address->id;
            }
        }
        //
        
        if ( $check_orders ) {
            $result = PedidoDetalle::where( 'pedido_id', $check_orders->id )->where( 'inventario_id', $id )->first(  );
            $exist = false;
            if ( $result ) {
                $exist = true;
            }
            return json_encode( $exist );
        } else {
            return json_encode( "No ORDER" );
        }
       
    }
}
