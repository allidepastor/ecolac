<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
use Auth;

class PedidoController extends Controller
{
	public function  __construct(  )
	{
		$this->middleware( 'order' );
	}
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index( Request $request )
    {

    }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
	//
      $in_order = Pedido::create( [
        'direccion_id' => $request->direccion_id,
        'session' => $request->session,
        'estado' => $request->estado,
		] );
		
         if( $in_order ) {
            return 'added';
         } else {
            return 'error';
        }    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show( $id, Request $request )
  {
    //
		$order = Pedido::with( 'pedidoDetalles.inventario.presentacion' )
			->with( 'pedidoDetalles.inventario.medida' )
			->with( 'pedidoDetalles.inventario.producto.tipo' )
			->with( 'pedidoDetalles.inventario.producto.categoria' )
			->with( 'direccion.user' )
			->where( 'id', $id )->first(  );

		if ( $request->wantsJson(  ) ) {
			return $order->toJson(  );
		}
	}

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
	//
		$in_order = Pedido::where( 'id', $id )->update( [
		 'direccion_id' => $request->direccion_id,
         'session' => $request->session,
         'estado' => $request->estado,
		] );
		if ( $in_order ) {        
		  return 'update';
		} else {
		  return 'error';
		}
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
	//
	$order = Pedido::find($id);
	if($order){
		if(isset($order->pedidoDetalles)) {
			$order->pedidoDetalles.inventario.product.tipo()->delete();
		}
		$order->delete();
		return 'done';
	} else{
		return 'error';
	}
  }
}