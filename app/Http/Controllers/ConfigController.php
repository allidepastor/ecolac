<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $configs = Config::all();
        //return view('productos.index',compact('productos'));
        if ( $request->wantsJson(  ) ) {
			return $productos->toJson(  );
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $name="";
        if($request->hasFile('favicon')){
            $file = $request->file('favicon');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/',$name);
            
        }
                
        $configs = new Config();
        $configs->nombre_sitio= $request->input('nombre_sitio');
        $configs->email  = $request->input('email');
        $configs->email_copia = $request->input('email_copia');
        $configs->favicom = $request->input('favicon');
        $configs->url_sitio = $request->input('url_sitio');
      
        $configs->save();
        

        return redirect(  )->back(  )->with( 'success', 'Configuraciones creadas correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Config $config)// exlicit binding
    {
        //$producto = Producto::find($id);
       // $producto = Producto::where('slug','=', $slug)->firstOrFail();
       $configs = Config::all();
        return view('productos.show',compact('configs') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        $configs = Config::all();
        return view('productos.edit',compact('config') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre_sitio' => 'required', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            'email' => 'required',
            'email_copia' => 'required',
            'favicon' => 'required',
            'url_sitio' => 'required',
        ] );
    
        $in_config = Config::where( 'id', $id )->update( [ // Se actualiza al usuario
            'nombre_sitio' => $request->nombre_sitio, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
            'email' => $request->email, // el nombre de la variable va de terminado por el nombre y id del input, ejemplo: el input de apellido es <input name="input" id="input".../>
            'email_copia' => $request->email_copia,
            'favicon' => $request->favicon,
            'url_sitio' => $request->url_sitio,
        ] );            
         
    
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado  correctamente' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $config = Producto::find( $id ); // Buscamos al producto por el id
        $configname = $config->nombre; // Guardamos el nombre del producto
        $config->delete(  ); // eliminamos al producto
        return redirect(  )->back(  )->with( 'success', 'ha sido eliminado correctamente ' ); // redirigimos a la pagina con un mensaje
    
    }
}
