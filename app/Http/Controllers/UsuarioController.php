<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre' => 'required|max:255', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            'apellido' => 'required|max:255',
            'cedula' => 'required|max:255',
            'telefono' => 'required|max:11',
            'email' => 'required|email|max:250',
            'password' => 'required',
            'rol' => 'required',
        ] );
        //
        $in_user = User::create( [ // Se crea el usuario
            'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
            'apellido' => $request->apellido, // el nombre de la variable va de terminado por el nombre y id del input, ejemplo: el input de appellido es <input name="input" id="input".../>
            'cedula' => $request->cedula,
            'telefono' => $request->telefono,
            'email' => $request->email,
            'password' => bcrypt( $request->password ), // usamos el metodo bycrypt para codificar la contrasenia            
          ] );
        $in_user->update( [ 'rol_id' => intval( $request->rol ) ] ); // Actualiamos el rol del usuario
       
                      
        return redirect(  )->back(  )->with( 'success', 'Usuario creado correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
          
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        //
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre' => 'required|max:255', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            'apellido' => 'required|max:255',
            'cedula' => 'required|max:255',
            'telefono' => 'required|max:11',
            'email' => 'required|email|max:250',
            'rol' => 'required',
        ] );
        //
        if ( $request->password ) { // en caso que se mande una nueva contrasenia, dado que la contrasenia, si se ve en la validacion ya no es un campo obligatorio
            $in_user = User::where( 'id', $id )->update( [ // Se actualiza al usuario
                'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de editar al usuario
                'apellido' => $request->apellido, // el nombre de la variable va de terminado por el nombre y id del input, ejemplo: el input de apellido es <input name="input" id="input".../>
                'cedula' => $request->cedula,
                'telefono' => $request->telefono,
                'email' => $request->email,
                'password' => bcrypt( $request->password ), // usamos el metodo bycrypt para codificar la contrasenia  
                'rol_id' => $request->rol,
              ] );              
              //
        } else {
            $in_user = User::where( 'id', $id )->update( [ // Se actualiza al usuario
                'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
                'apellido' => $request->apellido, // el nombre de la variable va de terminado por el nombre y id del input, ejemplo: el input de apellido es <input name="input" id="input".../>
                'cedula' => $request->cedula,
                'telefono' => $request->telefono,
                'email' => $request->email,
                'rol_id' => $request->rol,
            ] );            
            //
        }        
       
                      
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado al usuario correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido modificado en caso que el usuario se actualice.
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
        $user = User::find( $id ); // Buscamos al usuario por el id
        $username = $user->nombre; // Guardamos el nombre del usuario
        $user->delete(  ); // eliminamos al usuario
        return redirect(  )->back(  )->with( 'success', 'Se ha eliminado correctamente al usuario: '.$username ); // redirigimos a la pagina con un mensaje
    }
}
