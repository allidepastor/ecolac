<?php

namespace App\Http\Controllers;

use App\Pedido;
use Illuminate\Http\Request;
use App\PedidoDetalle;

class PedidoDetalleController extends Controller
{
  public function __construct(  )
  {
    $this->middleware("order");
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index( Request $request )
  {
    //
		$order_details = PedidoDetalle::with( 'inventario.presentacion' )
        ->with( 'pedidoDetalles.inventario.medida' )
        ->with( 'pedidoDetalles.inventario.producto.tipo' )
        ->with( 'pedidoDetalles.inventario.producto.categoria' )
        ->with( 'direccion.user' )->get( );

		if ( $request->wantsJson(  ) ) {
			return $order_details->toJson(  );
		}
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store( Request $request )
  {
		//
		$in_order = [];

		$check_exist = PedidoDetalle::where( 'inventario_id', $request->inventario_id )->where( 'pedido_id', $request->order->id )->first(  );

		if ( $check_exist ) {
			return 'exist';
		} else {
			$in_order = PedidoDetalle::create( [
				'pedido_id' => $request->order->id,
				'inventario_id' => $request->inventario_id,
				'cantidad' => $request->cantidad,
			] );
		}
		if ( $in_order ){
			return 'added';
		} else {
			return 'error';
		}
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show( $id, Request $request )
  {
		//
		$order_detail = PedidoDetalle::with( 'inventario.presentacion' )
            ->with( 'pedidoDetalles.inventario.medida' )
            ->with( 'pedidoDetalles.inventario.producto.tipo' )
            ->with( 'pedidoDetalles.inventario.producto.categoria' )
            ->with( 'direccion.user' )
			->where( 'id', $id )->first(  );

		if ( $request->wantsJson(  ) ) {
			return $order_detail->toJson(  );
		}
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit( $id, Request $request )
  {
    //

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
		return PedidoDetalle::where( 'id', $id )
			->update( ['cantidad' => $request->cantidad] );
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy( $id )
  {
    //
		return PedidoDetalle::where( 'id', $id )->delete(  );
  }
}