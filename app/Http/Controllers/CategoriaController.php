<?php

namespace App\Http\Controllers;

use App\Categoria;


use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $categorias = Categoria::all();
        //return view('productos.index',compact('productos'));
        if ( $request->wantsJson(  ) ) {
			return $categorias->toJson(  );
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
     
        $categorias = Categoria::all();
        return view('categorias.create', compact('categorias'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        
        
        $categorias = new Categoria();
        $categorias->nombre= $request->input('nombre');
         
        $categorias->save();
        

        return redirect(  )->back(  )->with( 'success', 'Categoria creada correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)// exlicit binding
    {   
        $categorias = Categoria::all();

        return view('categorias.show',compact('categorias') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categoria)
    {
       $categorias = Categoria::all();
        return view('categorias.edit', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre' => 'required', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            
           
        ] );
    
        $in_categoria = Categoria::where( 'id', $id )->update( [ // Se actualiza al usuario
            'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
                       
        ] );            
         
        
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado la categoria correctamente' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $categoria = Categoria::find( $id ); // Buscamos al categoria por el id
        $categorianame = $categoria->nombre; // Guardamos el nombre del categoria
        $categoria->delete(  ); // eliminamos al categoria
        return redirect(  )->back(  )->with( 'success', 'La categoria ha sido eliminada correctamente ' ); // redirigimos a la pagina con un mensaje
    
    }
}
