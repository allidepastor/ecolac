<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//use View;
class VendedorController extends Controller
{

    public function __construct(  ) {
        //
        $this->middleware( 'auth' );
    }

    public function index (  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('vendedor') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            return view( 'vendedor.dashboard', compact( 'user' ) ); // se redirige a la vista de dashboard
        }
        return abort(404); // En caso que el usuario no tenga el rol de admin, se le manda a la pagina 404
    }

}