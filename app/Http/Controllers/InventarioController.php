<?php

namespace App\Http\Controllers;

use App\Tipo;
use App\Inventario;
use App\Medida;
use App\Producto;
use App\Sucursal;
use App\Presentacion;

use Illuminate\Http\Request;

class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $inventarios = Inventario::with( 'producto' )->with( 'producto.tipo' )->with( 'producto.categoria' )->with( 'presentacion')->with( 'medida' )->inRandomOrder( )->get(  );
        //return view('productos.index',compact('productos'));
        if ( $request->wantsJson(  ) ) {
			return $inventarios->toJson(  );
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
     
        $presentacions= Presentacion::all();
        $productos= Producto::all();
        $medidas= Medida::all();
        $sucursals=Sucursals::all();
        
        return view('inventarios.create', compact('presentacions','productos','medidas','sucursals'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
       $name="";
        if($request->hasFile('imagen_url')){
            $file = $request->file('imagen_url');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/',$name);
            
        }
        
        $inventarios= new Inventario();
        $inventarios->producto_id= $request->input('producto');
        $inventarios->presentacion_id= $request->input('presentacion');
        $inventarios->medida_id= $request->input('medida');
        $inventarios->tamaño=$request->input('tamaño');
        $inventarios->precio=$request->input('precio');
        $inventarios->imagen_url=$request->input('imagen_url');
        $inventarios->sucursal_id=$request->input('sucursal');
        $inventarios->existencia=$request->input('existencia');
        $inventarios->disponible=$request->input('disponible');
   
        $inventarios->save();
        

        return redirect(  )->back(  )->with( 'success', 'Inventario creado correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Inventario $inventario)// exlicit binding
    {
   
       $presentacions= Presentacion::all();
       $productos= Producto::all();
       $medidas= Medida::all();
       $sucursals=Sucursals::all();
       $inventarios=Inventario::with('producto.tipo')->get();
        return view('inventarios.show',compact('presentacions', 'productos','medidas','sucursals') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventario $inventario)
    {
    
       $presentacions= Presentacion::all();
       $productos= Producto::all();
       $medidas= Medida::all();
       $sucursals=Sucursals::all();
        return view('inventarios.edit', compact('presentacions','productos','medidas','sucursals','inventario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'producto' => 'required', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            'presentacion' => 'required',
            'medida' => 'required',
            'tamaño' => 'required',
            'precio' => 'required',
            'imagen_url' => 'required',
            'sucursal' => 'required',
            'existencia' => 'required',
            'disponible' => 'required',
        ] );
    
        $in_producto = Producto::where( 'id', $id )->update( [ // Se actualiza al usuario
            'producto_id' => $request->producto, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
            'presentacion_id' => $request->presentacion,
            'medida_id' => $request->medida,
            'tamaño' => $request->tamaño,
            'precio' => $request->precio,
            'imagen_url' => $request->imagen_url,
            'sucursal_id' => $request->sucursal,
            'existencia' => $request->existencia,
            'disponible' => $request->disponible,
           
        ] );            
         
    
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado al inventario correctamente' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $inventario = Inventario::find( $id ); // Buscamos al producto por el id
        $inventarioname = $inventario->id; // Guardamos el nombre del 
        $inventario->delete(  ); // eliminamos al producto
        return redirect(  )->back(  )->with( 'success', 'El producto ha sido eliminado correctamente ' ); // redirigimos a la pagina con un mensaje
    
    }
}
