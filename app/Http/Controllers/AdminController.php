<?php

namespace App\Http\Controllers;

use Auth;
use App\Categoria;
use App\Producto;
use App\Tipo;
use App\Rol;
use App\User;
use App\Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//use View;
class AdminController extends Controller
{

    public function __construct(  ) {
        //
        $this->middleware( 'auth' );
    }

    public function index (  ) {
        //
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            return view( 'admin.dashboard', compact( 'user' ) ); // se redirige a la vista de dashboard
        }
        return abort(404); // En caso que el usuario no tenga el rol de admin, se le manda a la pagina 404

       
    }

    // Funcion para cargar la vista de crear usuario
    public function create_user(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $rols = Rol::all(  );
            return view( 'admin.crear_usuario', compact( 'user', 'rols' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }

    // Funcion para cargar la vista en la que se listan todos los usuarios
    public function list_users(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $rols = Rol::all(  );
            $users = User::all(  );
            return view( 'admin.lista_usuarios', compact( 'user', 'rols', 'users' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }

    // Funcion para cargar la vista con el formulario para editar un usuario
    public function edit_user( Request $request, $id ) { // $id toma el parametro de la url /admin/user/edit/1 <- donde uno seria el $id
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $rols = Rol::all(  );
            $usuario = User::where( 'id', $id )->first();
            return view( 'admin.editar_usuario', compact( 'user', 'rols', 'usuario' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }
    // Funcion para cargar la vista de crear producto
    public function create_product(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $producto
        if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $categorias = Categoria::all(  );
            $tipos = Tipo::all(  );
            return view( 'admin.crear_producto', compact( 'tipos','categorias' ) ); // se carga la vista que tiene el formulario para crear producto
        }
        return abort(404);
    }
    // Funcion para cargar la vista en la que se listan todos los productos
    public function list_products(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $categorias = Categoria::all(  );
            $tipos = Tipo::all(  );
            $productos = Producto::all(  );
            return view( 'admin.lista_productos', compact( 'productos', 'tipos','categorias' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }

    // Funcion para cargar la vista con el formulario para editar un producto
    public function edit_product( Request $request, $id ) { // $id toma el parametro de la url /admin/user/edit/1 <- donde uno seria el $id
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
            if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $categorias = Categoria::all(  );
            $tipos = Tipo::all(  );
            $producto = Producto::where( 'id', $id )->first();
            return view( 'admin.editar_producto', compact( 'producto', 'tipos','categorias' ) ); // se carga la vista que tiene el formulario para crear usuario
            }
             return abort(404);
        }
        // Funcion para cargar la vista en la que se listan CONFIGURACIONES DEL SISTEMA
    public function list_configs(  ) {
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
        if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $configs = Config::all(  );
            return view( 'admin.lista_configs', compact('configs' ) ); // se carga la vista que tiene el formulario para crear usuario
        }
        return abort(404);
    }

    // Funcion para cargar la vista con el formulario para editar un producto
    public function edit_config( Request $request, $id ) { // $id toma el parametro de la url /admin/user/edit/1 <- donde uno seria el $id
        $user = Auth::user(  ); // Se toma los datos de la sesion de usuario y se almacena en la variable $user
            if ( $user->hasRole('admin') ) { // Se verifica que el usuario tiene el rol de Admin
            // Aqui va el codigo si el usuario tiene el rol de Admin
            $config = Config::where( 'id', $id )->first();
            return view( 'admin.editar_config', compact( 'config' ) ); // se carga la vista que tiene el formulario para crear usuario
            }
             return abort(404);
        }
}