<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Direccion;
use Auth;

class DireccionController extends Controller
{
	public function  __construct(  )
	{
	}
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index( Request $request )
    {

    }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
    $user_id = null;
    if ( $request->user_id > 0 ) {
        $user_id = $request->user_id;
    }
    $this->validate( $request, [
        'nombre' => 'string|max:150',
        'email' => 'email|max:60',
        'ciudad' => 'required|string|max:150',
        'direccion' => 'required|string|max:150',

    ] );

    $in_direccion = Direccion::create( [
        'user_id' => $user_id,
        'nombre' => $request->nombre,
        'email' => $request->email,
        'ciudad' => $request->ciudad,
        'direccion' => $request->direccion,
        'latitud' => $request->latitud,
        'longitud' => $request->longitud,
        'estado' => true,
        'default' => false
    ] );
    
        if( $in_direccion ) {
        return $in_direccion->id;
        } else {
        return 'error';
    }    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show( $id, Request $request )
  {
    //
		$order = Pedido::with( 'pedidoDetalles.inventario.presentacion' )
			->with( 'pedidoDetalles.inventario.medida' )
			->with( 'pedidoDetalles.inventario.producto.tipo' )
			->with( 'pedidoDetalles.inventario.producto.categoria' )
			->with( 'direccion.user' )
			->where( 'id', $id )->first(  );

		if ( $request->wantsJson(  ) ) {
			return $order->toJson(  );
		}
	}

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
	//
	if ( Auth::check(  ) ) {
		$in_order = Pedido::where( 'id', $id )->update( [
		 'direccion_id' => $request->direccion_id,
         'session' => $request->session,
         'estado' => $request->estado,
		] );
		if ( $in_order ) {        
		  return 'update';
		} else {
		  return 'error';
		}
	  }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
	//
	$order = Pedido::find($id);
	if($order){
		if(isset($order->pedidoDetalles)) {
			$order->pedidoDetalles.inventario.product.tipo()->delete();
		}
		$order->delete();
		return 'done';
	} else{
		return 'error';
	}
  }
}