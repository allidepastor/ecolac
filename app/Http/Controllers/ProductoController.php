<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Tipo;
use App\Producto;

use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $productos = Producto::all();
        //return view('productos.index',compact('productos'));
        if ( $request->wantsJson(  ) ) {
			return $productos->toJson(  );
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tipos = Tipo::all();
        $categorias = Categoria::all();
        return view('productos.create', compact('categorias', 'tipos'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        /*$name="";
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/',$name);
            
        }
        */
        
        $productos = new Producto();
        $productos->nombre= $request->input('nombre');
        $productos->descripcion  = $request->input('descripcion');
        $productos->nota  = $request->input('nota');
        $productos->categoria_id = $request->input('categoria');
        $productos->tipo_id = $request->input('tipo');
      
        $productos->save();
        

        return redirect(  )->back(  )->with( 'success', 'Producto creado correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)// exlicit binding
    {
        //$producto = Producto::find($id);
       // $producto = Producto::where('slug','=', $slug)->firstOrFail();
       $tipos = Tipo::all();
        $categorias = Categoria::all();

        return view('productos.show',compact('producto', 'categorias', 'tipos') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        $tipos = Tipo::all();
        $categorias = Categoria::all();
        return view('productos.edit', compact('producto','tipos', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre' => 'required', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            'descripcion' => 'required',
            'categoria' => 'required',
            'tipo' => 'required',
           
        ] );
    
        $in_producto = Producto::where( 'id', $id )->update( [ // Se actualiza al usuario
            'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
            'descripcion' => $request->descripcion, // el nombre de la variable va de terminado por el nombre y id del input, ejemplo: el input de apellido es <input name="input" id="input".../>
            'categoria_id' => $request->categoria,
            'tipo_id' => $request->tipo,
           
        ] );            
         
    
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado al producto correctamente' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $producto = Producto::find( $id ); // Buscamos al producto por el id
        $productoname = $producto->nombre; // Guardamos el nombre del producto
        $producto->delete(  ); // eliminamos al producto
        return redirect(  )->back(  )->with( 'success', 'El producto ha sido eliminado correctamente ' ); // redirigimos a la pagina con un mensaje
    
    }
}
