<?php

namespace App\Http\Controllers;

use App\Presentacion;


use Illuminate\Http\Request;

class PresentacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $presentacion = Presentacion::all();
        //return view('productos.index',compact('productos'));
        if ( $request->wantsJson(  ) ) {
			return $presentacion->toJson(  );
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $presentacions = Presentacion::all();
        
        return view('presentacions.create', compact('presentacions'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        /*$name="";
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/',$name);
            
        }
        */
        
        $presentacions = new Presentacion();
        $presentacions->nombre= $request->input('nombre');
        $presentacions->notas  = $request->input('notas');
        $presentacions->save();
        

        return redirect(  )->back(  )->with( 'success', 'Presentacion creada correctamente' ); // redirigimos al formulario con el mensaje del usuario ha sido creado en caso que el usuario se cree.
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Presentacion $presentacion)// exlicit binding
    {
        //$producto = Producto::find($id);
       // $producto = Producto::where('slug','=', $slug)->firstOrFail();
       $presentacions = Presentacion::all();
        

        return view('presentacion.show',compact('presentacions') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Presentacion $presentacion)
    {
        $presentacions = Presentacion::all();

        return view('presentacions.edit', compact('presentacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->validate( request(  ), [ // Validar los campos. Ir a https://laravel.com/docs/6.x/validation por documentacion
            'nombre' => 'required', // En caso q alguna de estas reglas no se cumpla, se indicara en el formulario.
            'notas' => 'notas',
            
           
        ] );
    
        $in_presentacion = Presentacion::where( 'id', $id )->update( [ // Se actualiza al usuario
            'nombre' => $request->nombre, // $request es un arreglo que tiene los campos que le estamos pasando del formulario de crear usuario
            'notas' => $request->notas, // el nombre de la variable va de terminado por el nombre y id del input, ejemplo: el input de apellido es <input name="input" id="input".../>
            
           
        ] );            
         
        //$trainer->fill($request->all());
        /*$in_producto->fill($request->except('avatar'));
        $name="";
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $name = time().$file->getClientOriginalName();
            $in_producto->photo=$name;
            $file->move(public_path().'/images/',$name);
            
        }*/
        //$in_producto->save();
        return redirect(  )->back(  )->with( 'success', 'Se ha modificado la presentacion correctamente' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $presentacion = Presentacion::find( $id ); // Buscamos al producto por el id
        $presentacionname = $presentacion->nombre; // Guardamos el nombre del producto
        $presentacion->delete(  ); // eliminamos al producto
        return redirect(  )->back(  )->with( 'success', 'La presentacion ha sido eliminado correctamente ' ); // redirigimos a la pagina con un mensaje
    
    }
}
