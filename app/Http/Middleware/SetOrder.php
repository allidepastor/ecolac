<?php

namespace App\Http\Middleware;

use Closure;
use App\Pedido;
use Illuminate\Http\Request;
use Session;

class SetOrder
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    $sessionName = 'order_id';
    $order = [];
    //
    if ( \Session::has( $sessionName ) ) {
        $order_id = \Session::get($sessionName);
        $session_key = \Session::getId(  );
        $order = Pedido::findOrCreateById( intval( $order_id ), $session_key );
    } else { $order = Pedido::create( [ 'session' => '' ] ); }
   
    \Session::put( $sessionName, $order->id );

    $session_key = \Session::getId(  );
    Pedido::addSession( $order->id, $session_key );
    
    $request->order = $order;

    return $next($request);
  }
}