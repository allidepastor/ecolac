<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Pedido extends Model
{
    protected $table ='pedidos';

    protected $guarded = [  ];
    
    public function vendedor(  ) {
		return $this->belongsTo( 'App\User' );
    }

    public function direccion(  ) {
		return $this->belongsTo( 'App\Direccion' );
    }

    public function pedidoDetalles(  ) {
      return $this->hasMany( 'App\PedidoDetalle' );
    }

    public static function findOrCreateById( $order_id, $session_key ) {
      //echo $order_id; die();
      if ( $order_id > 0 ){
        $order = Pedido::where( 'id', $order_id )->first( );
        if ( $order ) {
          if ( $order->estado == 0 ) {
            return $order;
          } else {
            return Pedido::create( [ 'session' => $session_key ] );
          }
        }
      }
    }
  
    public static function addSession( $order_id, $session_key ) {
      Pedido::where( 'id', $order_id )->update( [ 'session'=> $session_key ] );
    }
  
    public function ordersCount(  ) {
      return $this->pedidoDetalles(  )->count(  );
    }

}
