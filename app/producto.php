<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = ['nombre', 'descripcion', 'nota', 'categoria_id', 'tipo_id' ];

    protected $guarded = [  ];

    public function tipo(  ) {
		return $this->belongsTo( 'App\Tipo' );
    }

    public function categoria(  ) {
		return $this->belongsTo( 'App\Categoria' );
    }

    public function inventarios(  ) {
		return $this->hasMany( 'App\Inventario' );
	}

  
}
