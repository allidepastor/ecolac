<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table ='direccions';

    protected $guarded = [  ];

    public function user(  ) {
		return $this->belongsTo( 'App\User' );
    }

    public function pedidos(  ) {
		return $this->hasMany( 'App\Pedido' );
	}
}
