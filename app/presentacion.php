<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentacion extends Model
{
    protected $table ='presentacions';

    protected $guarded = [  ];

    public function inventarios(  ) {
		return $this->hasMany( 'App\Inventario' );
	}
}
