<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table ='tipos'; //referencia a que tabla esta haciendo la relacion

    protected $guarded = [  ];    

    public function productos(  ) {
		return $this->hasMany( 'App\Producto' );
	}
}
