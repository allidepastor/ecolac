<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    protected $table ='medidas';

    protected $guarded = [  ];    

    public function inventarios(  ) {
		return $this->hasMany( 'App\Inventario' );
	}
}
