<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $table ='inventarios';

    protected $guarded = [  ];

    public function producto(  ) {
		return $this->belongsTo( 'App\Producto' );
    }
    
    public function medida(  ) {
		return $this->belongsTo( 'App\Medida' );
    }
    
    public function presentacion(  ) {
		return $this->belongsTo( 'App\Presentacion' );
    }
    
    public function pedidoDetalles(  ) {
		return $this->hasMany( 'App\PedidoDetalle' );
	}
}
