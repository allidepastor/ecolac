@extends('layouts.app')
@section('content')
<div class="row">
    
    @foreach ($productos as $producto)
        <div class="card col-md-3 text-center" style="margin:1%" >
            <img src="/images/{{$producto->photo}}" class="card-img-top" alt="{{$producto->nombre}}">
            <div class="card-body">
                <h5 class="card-title">{{$producto->nombre}}</h5>
                <!--<h5 class="">{{$producto->categoria_id}}</h5>
                -->
                <p class="card-text">{{$producto->descripcion}}</p>
            <a href="/productos/{{$producto->slug}}" class="btn btn-primary">Ver producto</a>
            </div>
        </div>
    @endforeach
</div>
@endsection
