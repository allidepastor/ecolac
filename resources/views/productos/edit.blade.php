@extends('layouts.app')
@section('content')

<div class="card mb-3" style="max-width: 80%;">
    <div class="row no-gutters">
      <div class="col-md-4">
        <img src="/images/{{$producto->photo}}"  class="card-img" alt="{{$producto->nombre}}">


    </div>
      <div class="col-md-8">
        <div class="card-body">
          
            <div class="form-group">
                <div class="sm">
            
                <form method="POST" action="/productos/{{$producto->slug}}"  enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label class="" for="" >Nombre producto</label>
                            <input class="" type="text" value="{{$producto->nombre}}" name="nombre">
                        </div>
                        <div class="form-group">
                            <select name="categoria">
                                @foreach ($categorias as $categoria)
                                    <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>                        
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="tipo">
                                @foreach ($tipos as $tipo)
                                    <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>                        
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="" for=""> Descripcion</label>
                            <input class="" value="{{$producto->descripcion}}" type="text" name="descripion">
                        </div>
                        <div class="form-group">
                            <label class="" for=""> Slug</label>
                            <input class="" value="{{$producto->slug}}" type="text" name="slug">
                        </div>
                        <div class="form-group">
                            <input  accept="image/*" type="file" name="photo">
                        </div>
            
                        
                        <button class="btn btn-primary" type="submit">Enviar</button>
                    </form>
                </div>
                
            </div>    

        </div>
      </div>
    </div>
  </div>


    
@endsection