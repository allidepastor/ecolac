@extends('layouts.app')
@section('content')
<div class="form-group">
    <div class="sm">
        <form method="POST" action="/productos"  enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="" for="" >Nombre producto</label>
                <input class="" type="text" name="nombre">
            </div>
            <div class="form-group">
                <select name="categoria">
                    @foreach ($categorias as $categoria)
                        <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>                        
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select name="tipo">
                    @foreach ($tipos as $tipo)
                        <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>                        
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="" for=""> Descripcion</label>
                <input class="" type="text" name="descripion">
            </div>
            <div class="form-group">
                <label class="" for=""> Slug</label>
                <input class="" type="text" name="slug">
            </div>
            <div class="form-group">
                <input  accept="image/*" type="file" name="photo">
            </div>

            
            <button class="btn btn-primary" type="submit">Enviar</button>
        </form>
    </div>
    
</div>    

    
@endsection