@extends('layouts.app')
@section('content')
<div class="card mb-3" style="max-width: 80%;">
    <div class="row no-gutters">
      <div class="col-md-4">
      <img src="/images/" class="card-img" alt="{{$producto->nombre}}">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">{{$producto->nombre}}</h5>
          <p class="card-text">{{$producto->descripcion}}</p>
          <p class="card-text">{{$producto->categoria->nombre}}</p>


        <p class="card-text"><small class="text-muted">
        @if (Auth::check())
        @if(Auth::user()->rol_id==5)
        <a href="/productos/{{$producto->id}}/edit">Añadir al carrito</a> </small></p>
        @else 
        <a href="/productos/{{$producto->id}}/edit">Eres un administrador Edita el producto</a> </small></p>
        @endif
        @else <a href="/productos/{{$producto->id}}/edit">Debe registrarse para poder comprar </a> </small></p>
        @endif
        </div>
      </div>
    </div>
  </div>
@endsection
