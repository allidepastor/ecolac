@extends('admin.layout')

@section('content')
<h1>Crear Producto</h1><br>
<div class="card">
    <div class="card-body">
        <div class="sm">
            {{-- ACA SE MUESTRAN EL MENSAJE CUANDO HAY ERRORES, ES DECIR NO SE CUMPLE ALGUNA DE LAS REGLAS DE VALIDACION --}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            {{-- EN CASO QUE TODO HAYA IDO BIEN SE MUESTRA UN MENSAJE NOTIFICANDO QUE EL producto FUE CREADO --}}
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
        <form method="POST" action="{{ URL::to('/productos') }}">
                @csrf
                <div class="form-group">
                    <label class="text-uppercase" for="nombre" >Nombre: </label>
                    <input class="form-control @error('nombre') is-invalid @enderror" value="{{ old('nombre') }}" type="text" name="nombre" id="nombre" required autocomplete="nombre" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                <label class="" for=""> Descripcion</label>
                <input class="form-control" type="text" name="descripion">
                </div>
                                
                <div class="form-group">
                    <label class="text-uppercase" for="nota" >Nota: </label>
                    <input class="form-control @error('nota') is-invalid @enderror" value="{{ old('nota') }}" type="text" name="nota" id="nota" required autocomplete="nota" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('nota')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="text-uppercase" for="categoria" >Seleccione una categoria: </label>
                    <select name="categoria" id="categoria" class="form-categoria">
                        @foreach ($categorias as $categoria)
                            <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>                        
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="text-uppercase" for="tipo" >Seleccione un tipo: </label>
                    <select name="tipo" id="tipo" class="form-tipo">
                        @foreach ($tipos as $tipo)
                            <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>                        
                        @endforeach
                    </select>
                </div>     

                       

                    <button class="btn btn-primary" type="submit">Crear Producto</button>
        </form>
        </div>
        
    </div>    
</div>
    
@endsection