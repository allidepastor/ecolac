@extends('admin.layout')

@section('content')
    <h1>PANEL DE ADMINISTRACIÓN</h1>

    <!-- /.col-md-6 -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">GESTION DE USUARIOS</h5>
            </div>
            <div class="card-body">
                <h6 class="card-title"></h6>

                <p class="card-text">Registro de nuevos usuarios administradores, vendedores, repartidores, clientes</p>
                
                <a href="{{ URL::to( '/admin/user/create' ) }}" class="btn btn-primary">IR</a> 
            </div>
        </div>

        <div class="card card-primary card-outline">
            <div class="card-header">
                <h5 class="m-0">GESTION DE PRODUCTOS</h5>
            </div>
            <div class="card-body">
                <h6 class="card-title"></h6>

                <p class="card-text">Crear nuevos productos de la empresa ECOLAC.</p>
                <a href="{{ URL::to( '/admin/product/create' ) }}" class="btn btn-primary">IR</a>
            </div>
        </div>
    </div>
    <!-- /.col-md-6 -->
@endsection