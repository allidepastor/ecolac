@extends('admin.layout')

@section('content')
<h1>Editar producto</h1><br>
<div class="card">
    <div class="card-body">
        <div class="sm">
            {{-- ACA SE MUESTRAN EL MENSAJE CUANDO HAY ERRORES, ES DECIR NO SE CUMPLE ALGUNA DE LAS REGLAS DE VALIDACION --}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            {{-- EN CASO QUE TODO HAYA IDO BIEN SE MUESTRA UN MENSAJE NOTIFICANDO QUE EL PRODUCTO FUE EDITADO --}}
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
        <form method="POST" action="{{ URL::to('/productos/'.$producto->id ) }}">
                {{ method_field('PUT') }}
                @csrf
                <div class="form-group">
                    <label class="text-uppercase" for="nombre" >Nombre: </label>
                    <input class="form-control @error('nombre') is-invalid @enderror" value="{{ $producto->nombre }}" type="text" name="nombre" id="nombre" required autocomplete="nombre" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="descripcion" >Descripcion: </label>
                    <input class="form-control @error('descripcion') is-invalid @enderror" value="{{ $producto->descripcion }}" type="text" name="descripcion" id="descripcion" required autocomplete="descripcion" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('descripcion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="categoria" >Seleccione una categoria: </label>
                    <select name="categoria" id="categoria" class="form-control">
                        @foreach ($categorias as $categoria)
                            @if ( $producto->categoria_id == $categoria->id ) {{-- COMPARAMOS categoria_ID QUE TIENE EL producto CON LOS categoria_id ID, Y EN CASO DE SER IGUAL DEJAMOS EL OPTION COMO SELECTED --}}
                                <option value="{{ $categoria->id }}" selected>{{ $categoria->nombre }}</option>
                            @else
                                <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <label class="text-uppercase" for="tipo" >Seleccione un tipo: </label>
                    <select name="tipo" id="tipo" class="form-control">
                        @foreach ($tipos as $tipo)
                            @if ( $producto->tipo_id == $tipo->id ) {{-- COMPARAMOS categoria_ID QUE TIENE EL producto CON LOS categoria_id ID, Y EN CASO DE SER IGUAL DEJAMOS EL OPTION COMO SELECTED --}}
                                <option value="{{ $tipo->id }}" selected>{{ $tipo->nombre }}</option>
                            @else
                                <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                
               
            
                <button class="btn btn-primary" type="submit">Actualizar producto</button>
            </form>
        </div>
        
    </div>    
</div>
    
@endsection