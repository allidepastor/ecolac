@extends('admin.layout')

@section('content')
<h1>Editar Configuraciones del Sistema</h1><br>
<div class="card">
    <div class="card-body">
        <div class="sm">
            {{-- ACA SE MUESTRAN EL MENSAJE CUANDO HAY ERRORES, ES DECIR NO SE CUMPLE ALGUNA DE LAS REGLAS DE VALIDACION --}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            {{-- EN CASO QUE TODO HAYA IDO BIEN SE MUESTRA UN MENSAJE NOTIFICANDO QUE EL PRODUCTO FUE EDITADO --}}
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
        <form method="POST" action="{{ URL::to('/configs/'.$config->id ) }}">
                {{ method_field('PUT') }}
                @csrf
                <div class="form-group">
                    <label class="text-uppercase" for="nombre_sitio" >Nombre del Sitio: </label>
                    <input class="form-control @error('nombre_sitio') is-invalid @enderror" value="{{ old('nombre_sitio') }}" type="text" name="nombre_sitio" id="nombre_sitio" required autocomplete="nombre_sitio" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('nombre_sitio')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                                                
                <div class="form-group">
                    <label class="text-uppercase" for="email" >Correo Electrónico: </label>
                    <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" type="text" name="email" id="email" required autocomplete="email" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label class="text-uppercase" for="email_copia" >Correo Electrónico: </label>
                    <input class="form-control @error('email_copia') is-invalid @enderror" value="{{ old('email_copia') }}" type="text" name="email_copia" id="email_copia" required autocomplete="email_copia" autofocus>
                    @error('email_copia')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input  accept="images" type="file" name="favicon">
                </div>                

                <div class="form-group">
                    <label class="text-uppercase" for="url_sitio" >URL del Sitio: </label>
                    <input class="form-control @error('url_sitio') is-invalid @enderror" value="{{ old('url_sitio') }}" type="text" name="url_sitio" id="url_sitio" required autocomplete="url_sitio" autofocus>
                        @error('url_sitio')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                
                <button class="btn btn-primary" type="submit">Actualizar Configuraciones</button>
        
            </form>
        </div>
        
    </div>    
</div>
    
@endsection