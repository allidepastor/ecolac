@extends('admin.layout')

@section('content')
<h1>Lista de usuarios</h1><br>
<div class="card">
    <div class="card-body">
        {{-- Cuando se elimine un usuario aca se pondra el mensaje --}}
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Cedula</th>
                <th>Telefono</th>
                <th>Correo</th>
                <th>Rol</th>
                <th colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $usuario)
              <tr>
                <td>{{ $usuario['id'] }}</td>
                <td>{{ $usuario['nombre'] }}</td>
                <td>{{ $usuario['apellido'] }}</td>
                <td>{{ $usuario['cedula'] }}</td>
                <td>{{ $usuario['telefono'] }}</td>
                <td>{{ $usuario['email'] }}</td>
                <td>
                    @foreach ($rols as $rol)
                        @if ( $usuario->rol_id == $rol->id ) {{-- COMPARAMOS EL ROL_ID QUE TIENE EL USUARIO CON LOS ROLES ID, Y EN CASO DE SER IGUAL DEJAMOS EL NOMBRE DEL ROL --}}
                           {{ $rol->nombre }}                        
                        @endif
                    @endforeach
                </td>
                <td><a href="{{ URL::to( '/admin/user/edit/'.$usuario['id'] ) }}" class="btn btn-warning">Editar</a></td>
                <td>
                  <form action="{{ action('UsuarioController@destroy', $usuario['id']) }}" method="POST">
                    {{ csrf_field(  ) }}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        
    </div>    
</div>
    
@endsection