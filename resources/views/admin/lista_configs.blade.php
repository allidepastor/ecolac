@extends('admin.layout')

@section('content')
<h1>Lista de Configuraciones</h1><br>
<div class="card">
    <div class="card-body">
        {{-- Cuando se elimine un producto aca se pondra el mensaje --}}
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre del Sitio</th>
                <th>Email</th>
                <th>Email con Copia</th>
                <th>Favicon</th>
                <th>URL del Sitio</th>
                <th colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($configs as $config)
              <tr>
                <td>{{ $config['id'] }}</td>
                <td>{{ $config['nombre_sitio'] }}</td>
                <td>{{ $config['email'] }}</td>
                <td>{{ $config['email_copia'] }}</td>
                <td>{{ $config['favicon'] }}</td>
                <td>{{ $config['url_sitio'] }}</td>
                <td><a href="{{ URL::to( '/admin/config/edit/'.$config['id'] ) }}" class="btn btn-warning">Editar</a></td>
                <td>
                  <form action="{{ action('ConfigController@destroy', $config['id']) }}" method="POST">
                    {{ csrf_field(  ) }}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        
    </div>    
</div>
    
@endsection