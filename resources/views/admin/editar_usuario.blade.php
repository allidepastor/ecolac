@extends('admin.layout')

@section('content')
<h1>Crear usuario</h1><br>
<div class="card">
    <div class="card-body">
        <div class="sm">
            {{-- ACA SE MUESTRAN EL MENSAJE CUANDO HAY ERRORES, ES DECIR NO SE CUMPLE ALGUNA DE LAS REGLAS DE VALIDACION --}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            {{-- EN CASO QUE TODO HAYA IDO BIEN SE MUESTRA UN MENSAJE NOTIFICANDO QUE EL USUARIO FUE CREADO --}}
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
        <form method="POST" action="{{ URL::to('/usuarios/'.$usuario->id ) }}">
                {{ method_field('PUT') }}
                @csrf
                <div class="form-group">
                    <label class="text-uppercase" for="nombre" >Nombre: </label>
                    <input class="form-control @error('nombre') is-invalid @enderror" value="{{ $usuario->nombre }}" type="text" name="nombre" id="nombre" required autocomplete="nombre" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="apellido" >Apellido: </label>
                    <input class="form-control @error('apellido') is-invalid @enderror" value="{{ $usuario->apellido }}" type="text" name="apellido" id="apellido" required autocomplete="apellido" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('apellido')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="cedula" >Cédula: </label>
                    <input class="form-control @error('cedula') is-invalid @enderror" value="{{ $usuario->cedula }}" type="text" name="cedula" id="cedula" required autocomplete="cedula" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('cedula')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="telefono" >Teléfono: </label>
                    <input class="form-control @error('telefono') is-invalid @enderror" value="{{ $usuario->telefono }}" type="text" name="telefono" id="telefono" required autocomplete="telefono" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('telefono')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="email" >Correo Electrónico </label>
                    <input class="form-control @error('email') is-invalid @enderror" value="{{ $usuario->email }}" type="email" name="email" id="email" required autocomplete="email" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="password" >Contraseña: </label>
                    <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="password" autocomplete="password" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="rol" >Seleccione un rol: </label>
                    <select name="rol" id="rol" class="form-control">
                        @foreach ($rols as $rol)
                            @if ( $usuario->rol_id == $rol->id ) {{-- COMPARAMOS EL ROL_ID QUE TIENE EL USUARIO CON LOS ROLES ID, Y EN CASO DE SER IGUAL DEJAMOS EL OPTION COMO SELECTED --}}
                                <option value="{{ $rol->id }}" selected>{{ $rol->nombre }}</option>
                            @else
                                <option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                
                            
                <button class="btn btn-primary" type="submit">Actualizar usuario</button>
            </form>
        </div>
        
    </div>    
</div>
    
@endsection