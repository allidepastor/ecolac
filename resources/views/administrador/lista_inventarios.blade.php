@extends('administrador.layout')

@section('content')
<h1>Lista de Inventario</h1><br>
<div class="card">
    <div class="card-body">
        {{-- Cuando se elimine un producto aca se pondra el mensaje --}}
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Producto</th>
                <th>Presentacion</th>
                <th>Medida</th>
                <th>Tamaño</th>
                <th>Precio</th>
                <th>Imagen</th>
                <th>Sucursal</th>
                <th>Existencia</th>
                <th>Disponible </th>
                <th colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($inventarios as $inventario)
              <tr>
                <td>{{ $inventario['id'] }}</td>
                <td>
                    @foreach ($productos as $producto)
                        @if ( $inventario->producto_id == $producto->id ) {{-- COMPARAMOS EL categoria_ID QUE TIENE --}}
                           {{ $producto->nombre }}                       
                        @endif
                        
                    @endforeach
                    
                </td>
               
                <td>
                    @foreach ($presentacions as $presentacion)
                        @if ( $inventario->presentacion_id == $presentacion->id ) {{-- COMPARAMOS EL categoria_ID QUE TIENE --}}
                           {{ $presentacion->nombre }}                        
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach ($medidas as $medida)
                        @if ( $inventario->medida_id == $medida->id ) {{-- COMPARAMOS EL categoria_ID QUE TIENE --}}
                           {{ $medida->nombre }}                        
                        @endif
                    @endforeach
                </td>
                <td>{{ $inventario['tamaño'] }}</td>
                <td>{{ $inventario['precio'] }}</td>
                <td>{{ $inventario['imagen_url'] }}</td>
                <td>
                  @foreach ($sucursals as $sucursal)
                      @if ( $inventario->sucursal_id == $sucursal->id ) {{-- COMPARAMOS EL categoria_ID QUE TIENE --}}
                         {{ $sucursal->nombre }}                        
                      @endif
                  @endforeach
                </td>
                <td>{{ $inventario['existencia'] }}</td>
                <td>{{ $inventario['disponible'] }}</td>
                
                
                <td><a href="{{ URL::to( '/administrador/inventario/edit/'.$inventario['id'] ) }}" class="btn btn-warning">Editar</a></td>
                <td>
                  <form action="{{ action('InventarioController@destroy', $inventario['id']) }}" method="POST">
                    {{ csrf_field(  ) }}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        
    </div>    
</div>
    
@endsection