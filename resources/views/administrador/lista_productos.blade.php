@extends('administrador.layout')

@section('content')
<h1>Lista de productos</h1><br>
<div class="card">
    <div class="card-body">
        {{-- Cuando se elimine un producto aca se pondra el mensaje --}}
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Categoria</th>
                <th>Tipo</th>
                
                <th colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($productos as $producto)
              <tr>
                <td>{{ $producto['id'] }}</td>
                <td>{{ $producto['nombre'] }}</td>
                <td>{{ $producto['descripcion'] }}</td>
                <td>
                    @foreach ($categorias as $categoria)
                        @if ( $producto->categoria_id == $categoria->id ) {{-- COMPARAMOS EL categoria_ID QUE TIENE --}}
                           {{ $categoria->nombre }}                        
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach ($tipos as $tipo)
                        @if ( $producto->tipo_id == $tipo->id ) {{-- COMPARAMOS EL categoria_ID QUE TIENE --}}
                           {{ $tipo->nombre }}                        
                        @endif
                    @endforeach
                </td>
                

                <td><a href="{{ URL::to( '/administrador/product/edit/'.$producto['id'] ) }}" class="btn btn-warning">Editar</a></td>
                <td>
                  <form action="{{ action('ProductoController@destroy', $producto['id']) }}" method="POST">
                    {{ csrf_field(  ) }}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        
    </div>    
</div>
    
@endsection