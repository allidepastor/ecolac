@extends('administrador.layout')

@section('content')
<h1>Lista de Categorias</h1><br>
<div class="card">
    <div class="card-body">
        {{-- Cuando se elimine un producto aca se pondra el mensaje --}}
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                
                <th colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($categorias as $categoria)
              <tr>
                <td>{{ $categoria['id'] }}</td>
                <td>{{ $categoria['nombre'] }}</td>
                
                <td><a href="{{ URL::to( '/administrador/categoria/edit/'.$categoria['id'] ) }}" class="btn btn-warning">Editar</a></td>
                <td>
                  <form action="{{ action('CategoriaController@destroy', $categoria['id']) }}" method="POST">
                    {{ csrf_field(  ) }}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                  </form>
                  
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        
    </div>    
</div>
    
@endsection