@extends('administrador.layout')

@section('content')
<h1>Crear Sucursal</h1><br>
<div class="card">
    <div class="card-body">
        <div class="sm">
            {{-- ACA SE MUESTRAN EL MENSAJE CUANDO HAY ERRORES, ES DECIR NO SE CUMPLE ALGUNA DE LAS REGLAS DE VALIDACION --}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            {{-- EN CASO QUE TODO HAYA IDO BIEN SE MUESTRA UN MENSAJE NOTIFICANDO QUE EL producto FUE CREADO --}}
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
        <form method="POST" action="{{ URL::to('/sucursals') }}">
                @csrf
                <div class="form-group">
                    <label class="text-uppercase" for="nombre" >Nombre: </label>
                    <input class="form-control @error('nombre') is-invalid @enderror" value="{{ old('nombre') }}" type="text" name="nombre" id="nombre" required autocomplete="nombre" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>       
                            
                <div class="form-group">
                    <label class="text-uppercase" for="repartidor" >Seleccione un Repartidor: </label>
                    <select name="repartidor" id="repartidor" class="form-user">
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}"> {{ $user->nombre }}</option>                        
                        @endforeach
                    </select>
                </div>
 

                <div class="form-group">
                    <label class="text-uppercase" for="ciudad" >Ciudad: </label>
                    <input class="form-control @error('ciudad') is-invalid @enderror" value="{{ old('ciudad') }}" type="text" name="ciudad" id="ciudad" required autocomplete="ciudad" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('ciudad')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="text-uppercase" for="longitud" >Longitud: </label>
                    <input class="form-control @error('longitud') is-invalid @enderror" value="{{ old('longitud') }}" type="text" name="longitud" id="longitud" required autocomplete="longitud" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('longitud')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="text-uppercase" for="latitud" >Latitud: </label>
                    <input class="form-control @error('Latitud') is-invalid @enderror" value="{{ old('latitud') }}" type="text" name="latitud" id="latitud" required autocomplete="latitud" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('latitud')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                    <button class="btn btn-primary" type="submit">Crear Sucursal</button>
        </form>
        </div>
        
    </div>    
</div>
    
@endsection