@extends('administrador.layout')

@section('content')
    <h1>PANEL DE ADMINISTRACIÓN</h1>

    <!-- /.col-md-6 -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">GESTION DE PRODUCTOS</h5>
            </div>
            <div class="card-body">
                <h6 class="card-title"></h6>

                <p class="card-text"></p>
                
                <a href="{{ URL::to( '/administrador/products' ) }}" class="btn btn-primary">IR</a> 
            </div>
        </div>

        <div class="card card-primary card-outline">
            <div class="card-header">
                <h5 class="m-0">GESTION DE VENTAS</h5>
            </div>
            <div class="card-body">
                <h6 class="card-title"></h6>

                <p class="card-text"></p>
                <a href="#" class="btn btn-primary">IR</a>
            </div>
        </div>
    </div>
    <!-- /.col-md-6 -->
@endsection