@extends('administrador.layout')

@section('content')
<h1>Crear categoria</h1><br>
<div class="card">
    <div class="card-body">
        <div class="sm">
            {{-- ACA SE MUESTRAN EL MENSAJE CUANDO HAY ERRORES, ES DECIR NO SE CUMPLE ALGUNA DE LAS REGLAS DE VALIDACION --}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            {{-- EN CASO QUE TODO HAYA IDO BIEN SE MUESTRA UN MENSAJE NOTIFICANDO QUE EL producto FUE CREADO --}}
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
        <form method="POST" action="{{ URL::to('/categorias') }}">
                @csrf
                <div class="form-group">
                    <label class="text-uppercase" for="nombre" >Nombre: </label>
                    <input class="form-control @error('nombre') is-invalid @enderror" value="{{ old('nombre') }}" type="text" name="nombre" id="nombre" required autocomplete="nombre" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>                    

                    <button class="btn btn-primary" type="submit">Crear categoria</button>
        </form>
        </div>
        
    </div>    
</div>
    
@endsection