@extends('administrador.layout')

@section('content')
<h1>Lista de Sucursales</h1><br>
<div class="card">
    <div class="card-body">
        {{-- Cuando se elimine un producto aca se pondra el mensaje --}}
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Repartidor</th>
                <th>Ciudad</th>
                <th>Longitud</th>
                <th>Latitud</th>
                
                <th colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($sucursals as $sucursal)
              <tr>
                <td>{{ $sucursal['id'] }}</td>
                <td>{{ $sucursal['nombre'] }}</td>
                <td>
                  @foreach ($users as $user)
                        @if ( $sucursal->user_id == $user->id ) {{-- COMPARAMOS EL categoria_ID QUE TIENE --}}
                           {{ $user->nombre }}                        
                        @endif
                        @endforeach
                </td>
                <td>{{ $sucursal['ciudad'] }}</td>
                <td>{{ $sucursal['longitud'] }}</td>
                <td>{{ $sucursal['latitud'] }}</td>
                
                <td><a href="{{ URL::to( '/administrador/sucursal/edit/'.$sucursal['id'] ) }}" class="btn btn-warning">Editar</a></td>
                <td>
                  <form action="{{ action('SucursalController@destroy', $sucursal['id']) }}" method="POST">
                    {{ csrf_field(  ) }}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        
    </div>    
</div>
    
@endsection