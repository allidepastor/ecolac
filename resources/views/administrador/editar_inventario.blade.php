@extends('administrador.layout')

@section('content')
<h1>Editar Inventario</h1><br>
<div class="card">
    <div class="card-body">
        <div class="sm">
            {{-- ACA SE MUESTRAN EL MENSAJE CUANDO HAY ERRORES, ES DECIR NO SE CUMPLE ALGUNA DE LAS REGLAS DE VALIDACION --}}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            {{-- EN CASO QUE TODO HAYA IDO BIEN SE MUESTRA UN MENSAJE NOTIFICANDO QUE EL PRODUCTO FUE EDITADO --}}
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
            @endif
        <form method="POST" action="{{ URL::to('/inventarios/'.$inventario->id ) }}">
                {{ method_field('PUT') }}
                @csrf
                <div class="form-group">
                    <label class="text-uppercase" for="producto" >Seleccione un Producto: </label>
                    <select name="producto" id="producto" class="form-producto">
                        @foreach ($productos as $producto)
                            <option value="{{ $producto->id }}">{{ $producto->nombre }}</option>                        
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <label class="text-uppercase" for="presentacion" >Seleccione una Presentacion: </label>
                    <select name="presentacion" id="presentacion" class="form-presentacion">
                        @foreach ($presentacions as $presentacion)
                            <option value="{{ $presentacion->id }}">{{ $presentacion->nombre }}</option>                        
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="text-uppercase" for="medida" >Seleccione una Medida: </label>
                    <select name="medida" id="medida" class="form-medida">
                        @foreach ($medidas as $medida)
                            <option value="{{ $medida->id }}">{{ $medida->nombre }}</option>                        
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="text-uppercase" for="tamaño" >Tamaño: </label>
                    <input class="form-control @error('tamaño') is-invalid @enderror" value="{{ old('tamaño') }}" type="text" name="tamaño" id="tamaño" required autocomplete="tamaño" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('tamaño')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                                                
                <div class="form-group">
                    <label class="text-uppercase" for="precio" >Precio: </label>
                    <input class="form-control @error('precio') is-invalid @enderror" value="{{ old('precio') }}" type="text" name="precio" id="precio" required autocomplete="precio" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('precio')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input  accept="image/*" type="file" name="imagen_url">
                </div>                

                <div class="form-group">
                    <label class="text-uppercase" for="sucursal" >Seleccione una Sucursal: </label>
                    <select name="sucursal" id="sucursal" class="form-sucursal">
                        @foreach ($sucursals as $sucursal)
                            <option value="{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>                        
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="text-uppercase" for="existencia" >Existencia: </label>
                    <input class="form-control @error('existencia') is-invalid @enderror" value="{{ old('existencia') }}" type="text" name="existencia" id="existencia" required autocomplete="existencia" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                        @error('existencia')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                
                <div class="form-group">
                    <label class="text-uppercase" for="disponible" >Disponible: </label>
                    <input class="form-control @error('disponible') is-invalid @enderror" value="{{ old('disponible') }}" type="text" name="disponible" id="disponible" required autocomplete="disponible" autofocus>
                    {{-- EN CASO QUE SE HAGA UNA VALIDACION, ACA SE DEVOLVERA EL MENSAJE INDICANCO QUE ESTA MAL --}}
                    @error('disponible')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                
               
            
                <button class="btn btn-primary" type="submit">Actualizar producto</button>
            </form>
        </div>
        
    </div>    
</div>
    
@endsection